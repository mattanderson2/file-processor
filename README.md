# File Processor Solution
### Matthew Anderson
#
For the File Processor problem, I used a class called FileDirectory to store the majority of the necessary 
logic for reading through the directories and writing to the .csv file.
I was unsure if a separate object was desired for this problem, and while it seemed rather unnecessary,
I went ahead and used it regardless. This included methods that checked if the file was a PDF or JPG, a method 
that processes the files and writes them to the .csv file, and a method
that generates an MD5 hash of the file. I was not familiar with generating MD5 hashes, and had to look up a 
solution from a secondary source, which is linked in the comments above the method.


A function independent of the FileDirectory class called getSubdirectories was implemented to find all 
the subDirectories that are included in the main directory. If the "r" flag is provided, then this function 
is performed and all subdirectories are searched and processed along with the main one.
I made ample use of lists throughout, and this may end up being to the program's detriment. There are a number of
nested for loops, which results in substandard runtime. Some restructuring would do well to fix these problems.

While the structure for this solution is fairly basic, it was challenging for me to decide exactly how I would like to structure
all the elements. I am not entirely convinced that the structure here is perfect, and that a better solution
is probably available. In all honesty, this problem was quite difficult for me in this regard. Finding the most optimal
solution to a problem has always been difficult for me, and refactoring can be a difficult process as well. If I had
more time as of writing this, I would most definitely like to rework my implementation. 