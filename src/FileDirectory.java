import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class representing a file directory. Will process the files in the directory and write all PDF and JPG files to
 * a .csv file.
 */
public class FileDirectory {
    private static final String PDF = "0x25504446";
    private static final String JPG = "0xFFD8";
    private final File[] fileContents;
    private final FileWriter results;


    FileDirectory(String mainDirectory, String outputFile) throws IOException {
        File file = new File(mainDirectory);
        fileContents = file.listFiles();
        results = new FileWriter(outputFile, true);
        results.write("");
    }

    // returns true if the filename begins with "0x25504446"
    public boolean isPDF(String file) {
        if(file.startsWith(PDF)) {
            return true;
        } else return false;

    }

    // returns true if the filename begins with "0xFFD8"
    public boolean isJPG(String file) {
        if(file.startsWith(JPG)) {
            return true;
        } else return false;
    }

    // Gets the MD5 hash of a file
    // solution found here: https://howtodoinjava.com/java/io/sha-md5-file-checksum-hash/
    public static String getFileChecksum(MessageDigest digest, File file) throws IOException {
        //Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(file);

        //Create byte array to read data in chunks
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        //Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        };

        //close the stream; We don't need it now.
        fis.close();

        //Get the hash's bytes
        byte[] bytes = digest.digest();

        //This bytes[] has bytes in decimal format;
        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length; i++)
        {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        //return complete hash
        return sb.toString();
    }


    // Process the file: if the file is a PDF or JPG, will write the file path, type and MD5 hash to the .csv file
    public void process() throws IOException, NoSuchAlgorithmException {
        for (File file : fileContents) {
            if (isJPG(file.getName()) || isPDF(file.getName())) {
                MessageDigest md = MessageDigest.getInstance("MD5");

                results.write(file.getAbsolutePath());
                results.write(",");
                if (isJPG(file.getName())) {
                    results.write("JPG");
                } else {
                    results.write("PDF");
                }
                results.write(",");
                results.write(getFileChecksum(md, file));
                results.write("\n");
            }
        }
    }

    // close files
    public void closeFiles() throws IOException {
        results.flush();
        results.close();
    }
}
