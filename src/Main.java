import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Main {

    // Returns a list of all subdirectories of the main directory
    static List<File> getSubdirectories(File file) {
        List<File> subdirectories = Arrays.asList(Objects.requireNonNull(file.listFiles(f -> f.isDirectory())));
        subdirectories = new ArrayList<File>(subdirectories);

        List<File> deepSubdirectories = new ArrayList<File>();
        for(File subdirectory : subdirectories) {
            deepSubdirectories.addAll(getSubdirectories(subdirectory));
        }
        subdirectories.addAll(deepSubdirectories);
        return subdirectories;
    }

    // Main program
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {

        // Accept command line arguments
        String flag = "";
        String directoryName = args[0];
        String outputFile = args[1];
        if (args.length == 3) {
             flag = args[2];
        }

        // Delete .csv file to be re-created
        File output = new File(outputFile);
        output.delete();

        // process files from main directory
        FileDirectory directory;
        directory = new FileDirectory(directoryName, outputFile);
        directory.process();
        directory.closeFiles();

        // If the r flag is included, search through all subdirectories
        if (flag.equals("r")) {
            ArrayList<File> subdirectories = new ArrayList();
            subdirectories = (ArrayList) getSubdirectories(new File(directoryName));
            for (File subdirectory : subdirectories) {
                directory = new FileDirectory(subdirectory.getAbsolutePath(), outputFile);
                directory.process();
                directory.closeFiles();
            }
        }
    }
}
