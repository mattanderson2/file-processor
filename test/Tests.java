import org.junit.Test;
import org.junit.Assert;
import static org.junit.Assert.*;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Tests {

    String mainDirectory = "C:\\Users\\matth\\Documents\\Bitbucket\\File-Processor\\files";
    String outputFile = "C:\\Users\\matth\\Documents\\Bitbucket\\File-Processor\\File-Processor-Result.csv";

    @Test
    public void testIsPDF_isTrue() throws IOException {

        String file = "0x25504446whatisloremipsum";
        FileDirectory directory = new FileDirectory("a.txt", "b.txt");

        assertTrue(directory.isPDF(file));
    }

    @Test
    public void testIsPDF_isFalse() throws IOException {

        String file = "0xFFD8-Screen Shot 2021-06-29 at 2.53.54 PM";
        FileDirectory directory = new FileDirectory("a.txt", "b.txt");

        assertFalse(directory.isPDF(file));
    }

    @Test
    public void testIsJPG_isTrue() throws IOException {

        String file = "0xFFD8-Screen Shot 2021-06-29 at 2.53.54 PM\"";
        FileDirectory directory = new FileDirectory("a.txt", "b.txt");

        assertTrue(directory.isJPG(file));
    }

    @Test
    public void testIsJPG_isFalse() throws IOException {

        String file = "0XFFD8-Screen Shot 2021-06-29 at 2.53.54 PM\"";
        FileDirectory directory = new FileDirectory("a.txt", "b.txt");

        assertFalse(directory.isJPG(file));
    }

    @Test
    public void testGetFileChecksum_TextFile_isEqual() throws IOException, NoSuchAlgorithmException {

        File file = new File("b.txt");
        MessageDigest md = MessageDigest.getInstance("MD5");
        FileDirectory directory = new FileDirectory("a.txt", "b.txt");

        String expected = "d41d8cd98f00b204e9800998ecf8427e";
        String actual = directory.getFileChecksum(md, file);

        assertEquals(expected, actual);
    }

    @Test
    public void testGetFileChecksum_README_isEqual() throws IOException, NoSuchAlgorithmException {

        File file = new File("README.md");
        MessageDigest md = MessageDigest.getInstance("MD5");
        FileDirectory directory = new FileDirectory("a.txt", "b.txt");

        String expected = "b335fd896a5b42c5dbd3a63b435a5dee";
        String actual = directory.getFileChecksum(md, file);

        assertEquals(expected, actual);
    }

    @Test
    public void testProcess() throws IOException, NoSuchAlgorithmException {

        // Arrange
        FileDirectory directory = new FileDirectory(mainDirectory, outputFile);
        FileReader fileReader = new FileReader(outputFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        directory.process();

        // Act
        String expected = "C:\\Users\\matth\\Documents\\Bitbucket\\File-Processor\\files\\0x25504446whatisloremipsum.pdf,PDF,20ee81a9c853a02d95cdfd9b27cb4a0a\n" +
                "C:\\Users\\matth\\Documents\\Bitbucket\\File-Processor\\files\\0xFFD8_Screen Shot 2021-06-29 at 2.52.50 PM.jpg,JPG,db3a3c5cb3089581a66ce927d2bd4d24\n" +
                "C:\\Users\\matth\\Documents\\Bitbucket\\File-Processor\\files\\84kas1\\0x25504446wheredoesitcomefrom.pdf,PDF,955e84aa39a55d5673c9f0e3d2ec4660\n" +
                "C:\\Users\\matth\\Documents\\Bitbucket\\File-Processor\\files\\84kas1\\4jasd1\\0xFFD8-Screen Shot 2021-06-29 at 2.53.54 PM.jpg,JPG,f1b57b68cdf00ff8842473828b67de08\n";
        StringBuffer actual = new StringBuffer();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            actual.append(line);
            actual.append("\n");
        }
        fileReader.close();

        // Assert
        assertEquals(expected, actual.toString());

    }
}
